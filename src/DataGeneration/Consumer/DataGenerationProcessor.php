<?php


namespace App\Hotels\DataGeneration\Consumer;


use App\Hotels\Core\Dto\HotelDto;
use App\Hotels\Core\Interfaces\KafkaProcessorInterface;
use Symfony\Component\Serializer\SerializerInterface;

class DataGenerationProcessor implements KafkaProcessorInterface {

  /**
   * @var SerializerInterface
   */
  private $serializer;

  /**
   * DataGenerationProcessor constructor.
   *
   * @param SerializerInterface $serializer
   */
  public function __construct(SerializerInterface $serializer) {
    $this->serializer = $serializer;
  }

  public function process(object $message): void {
    if (empty($message->payload)) {
      return;
    }
    try {
      /** @var HotelDto $payload */
      $payload = $this->serializer->deserialize(json_encode($message->payload), HotelDto::class, 'json');;
      echo $payload->getName();
    } catch (\Exception $exception) {
      echo($exception->getMessage());
      echo ($message->payload);
    }
  }
}