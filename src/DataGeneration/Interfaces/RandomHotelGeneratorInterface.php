<?php


namespace App\Hotels\DataGeneration\Interfaces;


use App\Hotels\Core\Dto\HotelDto;

interface RandomHotelGeneratorInterface {

  public function getHotel() : HotelDto;
}