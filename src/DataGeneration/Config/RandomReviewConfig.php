<?php


namespace App\Hotels\DataGeneration\Config;


use App\Hotels\Core\Interfaces\DateTimeHelperInterface;
use DateTimeImmutable;

class RandomReviewConfig {

  /**
   * @var DateTimeHelperInterface
   */
  private $dateTimeHelper;

  /**
   * @var DateTimeImmutable
   */
  private $startDateInterval;

  /**
   * @var DateTimeImmutable
   */
  private $endDateInterval;

  /**
   * @var int
   */
  private $maxReviewsPerHotel;

  /**
   * @var int
   */
  private $minReviewsByHotel;

  /**
   * @var int
   */
  private $maxInfoLength;


  /**
   * RandomReviewConfig constructor.
   *
   * @param DateTimeHelperInterface $dateTimeHelper
   * @param int|null                $startDate
   * @param int|null                $endDate
   * @param int|null                $maxReviewsPerHotel
   * @param int|null                $minReviewsByHotel
   * @param int|null                $maxInfoLength
   */
  public function __construct(DateTimeHelperInterface $dateTimeHelper, int $startDate, int $endDate, ?int $maxReviewsPerHotel = null, ?int $minReviewsByHotel = null, ?int $maxInfoLength = 50) {
    $this->startDateInterval  = $dateTimeHelper->getDate($startDate);
    $this->endDateInterval    = $dateTimeHelper->getDate($endDate);
    $this->maxReviewsPerHotel = $maxReviewsPerHotel;
    $this->minReviewsByHotel  = $minReviewsByHotel;
    $this->maxInfoLength = $maxInfoLength;
  }

  /**
   * @return DateTimeImmutable
   */
  public function getStartDateInterval(): DateTimeImmutable {
    return $this->startDateInterval;
  }

  /**
   * @return DateTimeImmutable
   */
  public function getEndDateInterval(): DateTimeImmutable {
    return $this->endDateInterval;
  }

  /**
   * @return int
   */
  public function getMaxReviewsPerHotel(): int {
    return $this->maxReviewsPerHotel;
  }

  /**
   * @param int $maxReviewsPerHotel
   */
  public function setMaxReviewsPerHotel(int $maxReviewsPerHotel): void {
    $this->maxReviewsPerHotel = $maxReviewsPerHotel;
  }

  /**
   * @return int
   */
  public function getMinReviewsByHotel(): int {
    return $this->minReviewsByHotel;
  }

  /**
   * @param int $minReviewsByHotel
   */
  public function setMinReviewsByHotel(int $minReviewsByHotel): void {
    $this->minReviewsByHotel = $minReviewsByHotel;
  }

  /**
   * @return int
   */
  public function getMaxInfoLength(): int {
    return $this->maxInfoLength;
  }

  /**
   * @param int $maxInfoLength
   */
  public function setMaxInfoLength(int $maxInfoLength): void {
    $this->maxInfoLength = $maxInfoLength;
  }
}
