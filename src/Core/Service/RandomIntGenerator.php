<?php


namespace App\Hotels\Core\Service;

use App\Hotels\Core\Interfaces\RandomIntGeneratorInterface;

class RandomIntGenerator implements RandomIntGeneratorInterface{


  /**
   * RandomIntGenerator constructor.
   */
  public function __construct() {
  }

  public function get(int $min, int $max): int {
    try {
      return random_int($min, $max);
    } catch (\Exception $e) {
      return $max;
    }
  }
}
