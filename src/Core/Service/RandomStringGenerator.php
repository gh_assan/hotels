<?php


namespace App\Hotels\Core\Service;

use App\Hotels\Core\Interfaces\RandomIntGeneratorInterface;
use App\Hotels\Core\Interfaces\RandomStringGeneratorInterface;

class RandomStringGenerator implements RandomStringGeneratorInterface {

  /**
   * @var RandomIntGeneratorInterface
   */
  private $randomIntGen;

  /**
   * RandomStringGenerator constructor.
   *
   * @param RandomIntGeneratorInterface $randomIntGen
   */
  public function __construct(RandomIntGeneratorInterface $randomIntGen) {
    $this->randomIntGen = $randomIntGen;
  }

  /**
   * @param int    $length
   * @param string $keys
   *
   * @return string
   */
  public function get(int $length, string $keys) : string {
    $len = strlen($keys);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
      $randomString .= $keys[$this->randomIntGen->get(0, $len - 1)];
    }
    return $randomString;
  }
}
