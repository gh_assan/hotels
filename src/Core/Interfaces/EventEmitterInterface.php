<?php


namespace App\Hotels\Core\Interfaces;


interface EventEmitterInterface {
  /**
   * @param EventInterface $event
   */
  public function emit(EventInterface $event): void;
}
