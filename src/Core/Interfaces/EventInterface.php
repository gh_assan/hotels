<?php


namespace App\Hotels\Core\Interfaces;


interface EventInterface {

  /**
   * @return string
   */
  public function getTopicName(): string;

  /**
   * @return string
   */
  public function getKey(): string;

  /**
   * @return object
   */
  public function getPayload(): object;
}
