<?php


namespace App\Hotels\Core\Interfaces;


interface KafkaProcessorInterface {
  public function process(object $message): void;
}