<?php


namespace App\Hotels\Core\Interfaces;


interface RandomStringGeneratorInterface {

  /**
   * @param int    $length
   *
   * @param string $keys
   *
   * @return mixed
   */
  public function get(int $length, string $keys) : string;
}