<?php


namespace App\Hotels\Consumer;


use App\Hotels\Core\Interfaces\ConsumerInterface;
use App\Hotels\Core\Interfaces\KafkaProcessorInterface;
use RdKafka\Conf;
use RdKafka\Exception;
use RdKafka\KafkaConsumer;

class Consumer implements ConsumerInterface{

  /**
   * @var ConsumerConfig
   */
  private $config;

  /**
   * @var KafkaConsumer
   */
  private $_consumer;

  /**
   * @var KafkaProcessorInterface
   */
  private $processor;

  /**
   * Consumer constructor.
   *
   * @param ConsumerConfig          $config
   * @param KafkaProcessorInterface $processor
   */
  public function __construct(ConsumerConfig $config, KafkaProcessorInterface $processor) {
    $this->config = $config;
    $this->_consumer = $this->getKafkaConsumer($config);
    $this->processor = $processor;
  }

  public function subscribe(string $topicName): void {
    try {
      $this->_consumer->subscribe([$topicName]);
    } catch (Exception $e) {
    }
  }

  /**
   * @param ConsumerConfig $consumerConfig
   *
   * @return KafkaConsumer
   */
  private function getKafkaConsumer(ConsumerConfig $consumerConfig) : KafkaConsumer{

    $conf = $this->getKafkaConfig($consumerConfig);
    return new KafkaConsumer($conf);
  }

  /**
   * @param ConsumerConfig $consumerConfig
   *
   * @return Conf
   */
  private function getKafkaConfig(ConsumerConfig $consumerConfig) : Conf{
    $conf = new Conf();
    $conf->set('group.id', $consumerConfig->getConsumerGroup());
    $conf->set('metadata.broker.list', $consumerConfig->getBrokerList());
    $conf->set('auto.offset.reset', 'smallest');

    return $conf;
  }

  public function consume() {
    while (true) {
      try {
        $message = $this->_consumer->consume(.5 * 1000);
        $this->processor->process($message);

      } catch (Exception $e) {
        $messages[] = $e->getMessage();
      }
    }
  }
}