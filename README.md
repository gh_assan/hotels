## Hotels and Reviews

This is the repository for Hotels And Reviews small project

### Project Design

Diagram to be added later 

Design high overview 

1- Console command to generate events to Kafka, with random Hotels and reviews
2- Kafka Consumer to consume the massages in Parallel and insert the data into the database
3- Console command to aggregate the data into stat tables
4- Api 
5- Tests either unit test or integration tests based on the remaining time
6- Load test  


### Project Estimated time

Infrastructure -- > 1 day 
Data Producer to Kafka -- > .5 day 
Kafka Consumer -- > .5 day
API endpoint -- > .5 day 
Unit Tests -- > .25 day 
Swagger Doc -- > .125 day 
Add code Quality Tools -- > .5 day 
Load tests  --> .5 day



### Links 

Control Center  http://localhost:9021/

